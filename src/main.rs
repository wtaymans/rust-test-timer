use structopt::StructOpt;

use core::f32::consts::PI;
use instant::Duration;
use nix::time::{ clock_gettime };
use nix::sys::time::{ TimeSpec };
use nix::sys::timerfd::{ TimerFd, ClockId, Expiration, TimerFlags, TimerSetTimeFlags };

use alsa::{Direction, ValueOr};
use alsa::pcm::{PCM, HwParams, Format, Access, Frames};
use alsa::direct::pcm::{MmapPlayback};

#[derive(Debug, Default)]
struct DLL {
    bw: f32,
    z1: f32, z2: f32, z3: f32,
    w0: f32, w1: f32, w2: f32,
}

impl DLL {
    fn set_bw(&mut self, bw: f32, period: f32, rate: f32) {
        let w : f32 = 2.0 * PI as f32 * bw * period / rate;
        self.w0 = 1.0 - (-20.0 * w).exp();
        self.w1 = w * 1.5 / period;
        self.w2 = w / 1.5;
        self.bw = bw;
    }
    fn update(&mut self, err: f32) -> f32 {
        self.z1 += self.w0 * (self.w1 * err - self.z1);
        self.z2 += self.w0 * (self.z1 - self.z2);
        self.z3 += self.w2 * self.z2;
        1.0 - (self.z2 + self.z3)
    }
}

struct Context {
    period: Frames,
    rate: u32,

    pcm: PCM,
    mmap: MmapPlayback<i32>,

    timerfd: TimerFd,

    max_error: i64,
    accumulator: f32,

    next_time: TimeSpec,

    dll: DLL,
}

#[derive(Debug, StructOpt)]
#[structopt(name = "pipewire", about = "PipeWire daemon")]
struct Opt {
    #[structopt(short = "V", long = "version", help = "Show version")]
    version: bool,
    #[structopt(short = "v", long = "verbose",
                help = "Increase debug", parse(from_occurrences))]
    verbose: u8,
}

fn write_period(context: &mut Context)
{
    context.mmap.write(&mut
        (0..context.period * 2).into_iter().map(|x| {
            if x % 2 == 0 {
                let pi2 = 2.0 * PI;
                let rate = context.rate as f32;
                context.accumulator += pi2 * 440.0 / rate;
                if context.accumulator >= pi2 {
                    context.accumulator -= pi2;
                }
            }
            let v = context.accumulator.sin() * 0x7fffffff as f32;
            v as i32
        }));
}

fn on_timer_wakeup(context: &mut Context)
{

    let avail: Frames = context.mmap.avail();
    let delay: Frames = context.mmap.buffer_size() - avail;

    let error = (delay - 1024).clamp(-context.max_error, context.max_error);

    /* update the dll with the error, this gives a rate correction */       
    let corr = context.dll.update(error as f32);                              

    println!("wakeup {avail} {delay:04} {error:3.4} {corr:3.8}");

    // set our new adjusted timeout. alternatively, this value can
    // instead be used to drive a resampler if this device is
    // slaved.
    context.next_time = context.next_time +
                            TimeSpec::from(
                                Duration::from_nanos(
                                    (context.period as f32 /
                                     corr * 1e9 / context.rate as f32) as u64));
    context.timerfd.set(Expiration::OneShot(context.next_time),
             TimerSetTimeFlags::TFD_TIMER_ABSTIME).unwrap();

    // pull in new samples write a new period
    write_period(context);
}

fn main() {
    let opt = Opt::from_args();
    let device: String = "hw:1".to_string();
    let period: Frames = 1024;
    let format: Format = Format::s32();
    let rate: u32 = 44100;
    let channels: u32 = 2;

    if opt.version {
    }
    else if opt.verbose != 0 {
    }

    let pcm = PCM::new(&device, Direction::Playback, false).unwrap();

    {
        let hwp = HwParams::any(&pcm).unwrap();
        hwp.set_access(Access::MMapInterleaved).unwrap();
        hwp.set_format(format).unwrap();
        hwp.set_channels(channels).unwrap();
        hwp.set_rate(rate, ValueOr::Nearest).unwrap();
        hwp.set_period_size(period, ValueOr::Nearest).unwrap();
        pcm.hw_params(&hwp).unwrap();
    }
    let mmap: MmapPlayback<i32> = pcm.direct_mmap_playback().unwrap();

    let timerfd = TimerFd::new(ClockId::CLOCK_MONOTONIC, TimerFlags::empty()).unwrap();

    let mut dll: DLL = Default::default();
    dll.set_bw(0.064, period as f32, rate as f32);

    let now = clock_gettime(nix::time::ClockId::CLOCK_MONOTONIC).unwrap();

    let mut context = Context {
        period: period,
        rate: rate,
        pcm: pcm,
        mmap: mmap,
        timerfd: timerfd,
        max_error: 255,
        accumulator: 0.0,
        next_time: now,
        dll: dll,
    };

    context.pcm.prepare().unwrap();

    /* before we start, write one period */
    write_period(&mut context);

    /* set our first timeout for now */
    context.timerfd.set(Expiration::OneShot(context.next_time),
                TimerSetTimeFlags::TFD_TIMER_ABSTIME).unwrap();

    context.pcm.start().unwrap();

    // wait for timer to expire and call the wakeup function,
    // this can be done in a poll loop as well */
    loop {
        context.timerfd.wait().unwrap();
        on_timer_wakeup(&mut context);
    }
}
